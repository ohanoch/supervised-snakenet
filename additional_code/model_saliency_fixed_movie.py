from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

import matplotlib
from matplotlib import pylab as P
import numpy as np
import pickle
import sys

#from saliency.saliency.guided_backprop import GuidedBackprop
#import saliency.saliency.visualization

#from saliency.guided_backprop import GuidedBackprop
#import saliency.visualization

import saliency

from test_board import drawBoard
#################################################################################################

# Boilerplate methods.
def ShowImage(im, title='', ax=None):
	if ax is None:
	  P.figure()
	P.axis('off')
	im = ((im + 1) * 127.5).astype(np.uint8)
	P.imshow(im)
	P.title(title)

def ShowGrayscaleImage(im, title='', ax=None):
	if ax is None:
	  P.figure()
	P.axis('off')

	P.imshow(im, cmap=P.cm.gray, vmin=0, vmax=1)
	P.title(title)

def ShowDivergingImage(grad, title='', percentile=99, ax=None):  
	if ax is None:
	  fig, ax = P.subplots()
	else:
	  fig = ax.figure
  
	P.axis('off')
	divider = make_axes_locatable(ax)
	cax = divider.append_axes('right', size='5%', pad=0.05)
	im = ax.imshow(grad, cmap=P.cm.coolwarm, vmin=-1, vmax=1)
	fig.colorbar(im, cax=cax, orientation='vertical')
	P.title(title)
#################################################################################################


def straightGenerator(batchData, mini_batch_size): #border different color
	for batchStartIndex in range(0,len(batchData[0]), mini_batch_size):
		batchEndIndex = batchStartIndex + min(mini_batch_size, len(batchData[0]) - batchStartIndex)
		batch = [batchData[i][batchStartIndex:batchEndIndex] for i in range(2)]
		yield [\
			[np.array([\
				[pixel if (pixel == 1 or pixel == 2 or pixel == 3) else 0 for pixel in board],\
				[pixel if (pixel == 4 or pixel == 5) else 0 for pixel in board],\
				[pixel if (pixel == 6 or pixel == 7) else 0 for pixel in board]\
				]).flatten() for board in batch[0]], \
			batch[1]]


def leftGenerator(batchData, mini_batch_size): #border different color
	for batchStartIndex in range(0,len(batchData[0]), mini_batch_size):
		batchEndIndex = batchStartIndex + min(mini_batch_size, len(batchData[0]) - batchStartIndex)
		batch = [batchData[i][batchStartIndex:batchEndIndex] for i in range(2)]
		yield [\
			[np.array([\
				[pixel if (pixel == 1 or pixel == 2 or pixel == 3) else 0 for pixel in board],\
				[pixel if (pixel == 4 or pixel == 5) else 0 for pixel in board],\
				[pixel if (pixel == 6 or pixel == 7) else 0 for pixel in board]\
				]).flatten() for board in batch[0]], \
			batch[1]]


def rightGenerator(batchData, mini_batch_size): #border different color
	for batchStartIndex in range(0,len(batchData[0]), mini_batch_size):
		batchEndIndex = batchStartIndex + min(mini_batch_size, len(batchData[0]) - batchStartIndex)
		batch = [batchData[i][batchStartIndex:batchEndIndex] for i in range(2)]
		yield [\
			[np.array([\
				[pixel if (pixel == 1 or pixel == 2 or pixel == 3) else 0 for pixel in board],\
				[pixel if (pixel == 4 or pixel == 5) else 0 for pixel in board],\
				[pixel if (pixel == 6 or pixel == 7) else 0 for pixel in board]\
				]).flatten() for board in batch[0]], \
			batch[1]]

def drawBoard(board):
	for i in range(2704):
		if board[i] == 0:
			print(" ", end=" ")
		else:
			print(int(board[i]), end=" ")

		if (i%52==51):
		   print("")

def generator(image):
	return np.reshape(np.array([\
		[pixel if (pixel == 1 or pixel == 2 or pixel == 3) else 0 for pixel in image],\
		[pixel if (pixel == 4 or pixel == 5) else 0 for pixel in image],\
		[pixel if (pixel == 6 or pixel == 7) else 0 for pixel in image]\
		]).transpose(), (52,52,3))

graph = tf.Graph()
with graph.as_default():
	# Restore the checkpoint
	config = tf.ConfigProto(allow_soft_placement=True)
	sess = tf.Session(config=config)#, graph=graph)
	saver = tf.train.import_meta_graph('/mnt/or-ext/collections/dean/model/output_snake_model_fixed_genie_20180703_103126-143200.meta')
	#saver = tf.train.Saver()
	saver.restore(sess, tf.train.latest_checkpoint('/mnt/or-ext/collections/dean/model'))

	# Construct the scalar neuron tensor.
	x = graph.get_tensor_by_name("x:0")
	keep_prob = graph.get_tensor_by_name("keep_prob:0")
	y_conv = graph.get_tensor_by_name('fc2/Add:0')
	neuron_selector = tf.placeholder(tf.int32)
	y = y_conv[0][neuron_selector]
	print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa     " +str(y_conv))    
	print("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb     " +str(neuron_selector))   
	print("cccccccccccccccccccccccccccccccccccc     " +str(y))   

	# Construct tensor for predictions.
	#result_argmax = graph.get_tensor_by_name("result_argmax:0")
	prediction = tf.argmax(y_conv, 0)
  


	#dataStraight = [np.load(fileLocation + "validationData/" + 'validationDataBoardsStraight' + str(1) + ".npy"), np.load(fileLocation + "validationData/" + 'validationDataMovesStraight' + str(1) + ".npy")]
	#dataLeft = [np.load(fileLocation + "validationData/"+ 'validationDataBoardsLeft' + str(1) + ".npy"), np.load(fileLocation + "validationData/" + 'validationDataMovesLeft' + str(1) + ".npy")]
	#dataRight = [np.load(fileLocation + "validationData/"+ 'validationDataBoardsRight' + str(1) + ".npy"), np.load(fileLocation + "validationData/"  + 'validationDataMovesRight' + str(1) + ".npy")]

	#straightBoard = straightGenerator(dataStraight, 1)
	#leftBoard = leftGenerator(dataLeft, 1)
	#rightBoard = rightGenerator(dataRight, 1)

	board_file = np.load("/mnt/or-ext/collections/dean/saliency_boards/" + 'boards.npy')

	for board_index in range(int(sys.argv[1]), int(sys.argv[1])+25):

		im = generator(board_file[board_index]) #2000
		yellow_im = [[np.array([1,1,0]) if list(p).count(0)==3 else p for p in row]for row in im]

		prediction_class = sess.run(prediction, feed_dict = {x: [im], keep_prob: 1.0})[0]
		#true_x = sess.run(x, feed_dict = {x: [im], keep_prob: 1.0})#, neuron_selector: prediction_class})
		#print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz   " + str(im_deflat))
		# Construct the saliency object. This doesn't yet compute the saliency mask, it just sets up the necessary ops.

		#drawBoard(board_file[board_index]) #2000


		font = {'family' : 'normal',
			'weight' : 'normal',
			'size'   : 12}

		matplotlib.rc('font', **font)

		ROWS = 1
		COLS = 5
		UPSCALE_FACTOR = 20
		P.figure(figsize=(ROWS * UPSCALE_FACTOR, COLS * UPSCALE_FACTOR))
		P.subplots_adjust(left=0.02, bottom=0.02, right=0.98, top=0.98, wspace=0.01, hspace=0.01)
		ShowGrayscaleImage(yellow_im, title='Original\nBoard', ax=P.subplot(ROWS, COLS, 1))

		
		#gradient_saliency = saliency.GradientSaliency(graph, sess, y, x)
		# Compute the vanilla mask and the smoothed mask.
		#vanilla_mask_3d = gradient_saliency.GetMask(im, feed_dict = {keep_prob: 1.0, neuron_selector: prediction_class})
		#smoothgrad_mask_3d = gradient_saliency.GetSmoothedMask(im, feed_dict = {keep_prob: 1.0, neuron_selector: prediction_class})

		# Call the visualization methods to convert the 3D tensors to 2D grayscale.
		#vanilla_mask_grayscale = saliency.VisualizeImageGrayscale(vanilla_mask_3d)
		#smoothgrad_mask_grayscale = saliency.VisualizeImageGrayscale(smoothgrad_mask_3d)
		

			# Render the saliency masks.
		#ShowGrayscaleImage(vanilla_mask_grayscale, title='Vanilla\nGradient', ax=P.subplot(ROWS, COLS, 2))
		#ShowGrayscaleImage(smoothgrad_mask_grayscale, title='Smooth-\nGrad', ax=P.subplot(ROWS, COLS, 3))
		
		print("end of code 1")
	 
		   
		guided_backprop = saliency.GuidedBackprop(graph, sess, y, x)
		#vanilla_guided_backprop_mask_3d = guided_backprop.GetMask(im, feed_dict = {keep_prob: 1.0, neuron_selector: prediction_class})
		smoothgrad_guided_backprop_mask_3d = guided_backprop.GetSmoothedMask(im, feed_dict = {keep_prob: 1.0, neuron_selector: prediction_class})
		
		#vanilla_mask_grayscale = saliency.VisualizeImageGrayscale(vanilla_guided_backprop_mask_3d)
		smoothgrad_mask_grayscale = saliency.VisualizeImageGrayscale(smoothgrad_guided_backprop_mask_3d)

		#ShowGrayscaleImage(vanilla_mask_grayscale, title='Vanilla\nGuided\nBackprop', ax=P.subplot(ROWS, COLS, 4))
		ShowGrayscaleImage(smoothgrad_mask_grayscale, title='SmoothGrad\nGuided\nBackprop', ax=P.subplot(ROWS, COLS, 2))
		ShowGrayscaleImage([[p1 * p2 for p1,p2 in zip(r1,r2)] for r1,r2 in zip(smoothgrad_mask_grayscale,yellow_im)], title='SmoothGrad\nGuided\nBackprop\nx Original', ax=P.subplot(ROWS, COLS, 3))
		
		print("end of code 2")

		integrated_gradients = saliency.IntegratedGradients(graph, sess, y, x)
		baseline = np.zeros(im.shape)
		baseline.fill(-1)

		#vanilla_integrated_gradients_mask_3d = integrated_gradients.GetMask(im, feed_dict = {keep_prob: 1.0, neuron_selector: prediction_class}, x_steps=25, x_baseline=baseline)
		smoothgrad_integrated_gradients_mask_3d = integrated_gradients.GetSmoothedMask(im, feed_dict = {keep_prob: 1.0, neuron_selector: prediction_class}, x_steps=25, x_baseline=baseline)

		#vanilla_mask_grayscale = saliency.VisualizeImageGrayscale(vanilla_integrated_gradients_mask_3d)
		smoothgrad_mask_grayscale = saliency.VisualizeImageGrayscale(smoothgrad_integrated_gradients_mask_3d)

		#ROWS = 1
		#COLS = 2
		#UPSCALE_FACTOR = 10
		#P.figure(figsize=(ROWS * UPSCALE_FACTOR, COLS * UPSCALE_FACTOR))

		#ShowGrayscaleImage(vanilla_mask_grayscale, title='Vanilla\nIntegrated\nGradients', ax=P.subplot(ROWS, COLS, 6))
		ShowGrayscaleImage(smoothgrad_mask_grayscale, title='Smoothgrad\nIntegrated\nGradients', ax=P.subplot(ROWS, COLS, 4))
		ShowGrayscaleImage([[p1 * p2 for p1,p2 in zip(r1,r2)] for r1,r2 in zip(smoothgrad_mask_grayscale,yellow_im)], title='Smoothgrad\nIntegrated\nGradients\nx Original', ax=P.subplot(ROWS, COLS, 5))

		#P.tight_layout()
		#P.show()
		P.savefig('/mnt/or-ext/collections/dean/saliency_images3/board' + str(board_index) + '.png', bbox_inches='tight')
		print("saved board " + str(board_index))


"""

neuron_selector = tf.placeholder(tf.int32)


dataFileNumber = 14
fileLocation = "/mnt/snake/snakeNN/snakeNN_data"+ str(dataFileNumber) +"/"

dataStraight = [np.load(fileLocation + "validationData/" + 'validationDataBoardsStraight' + str(1) + ".npy"),
	     np.load(fileLocation + "validationData/" + 'validationDataMovesStraight' + str(1) + ".npy")]
dataLeft = [np.load(fileLocation + "validationData/"+ 'validationDataBoardsLeft' + str(1) + ".npy"),
	     np.load(fileLocation + "validationData/" + 'validationDataMovesLeft' + str(1) + ".npy")]
dataRight = [np.load(fileLocation + "validationData/"+ 'validationDataBoardsRight' + str(1) + ".npy"),
         np.load(fileLocation + "validationData/"  + 'validationDataMovesRight' + str(1) + ".npy")]


config = tf.ConfigProto(allow_soft_placement=True)
with tf.Session(config=config) as sess:

    print("importing model...")
    saver = tf.train.import_meta_graph('/home/snake/data/snakeNN/presentation/snakeNN_0/server/models/output_snake_model_20171119_034434-33600.meta')
    saver.restore(sess, tf.train.latest_checkpoint('/home/snake/data/snakeNN/presentation/snakeNN_0/server/models'))

    #saver = tf.train.import_meta_graph('/home/snake/data/snakeNN/presentation/snakeNN_0/server/models/output_snake_model_20171119_034434-33600.meta')
    #saver.restore(sess, tf.train.latest_checkpoint('/home/snake/data/snakeNN/presentation/snakeNN_0/server/models'))

    print("setting up graph variables...")
    graph = tf.get_default_graph()
    x = graph.get_tensor_by_name("x:0")
    y_conv = graph.get_tensor_by_name('y_conv:0')

    straightBoard = straightGenerator(dataStraight, 1)
    leftBoard = leftGenerator(dataLeft, 1)
    rightBoard = rightGenerator(dataRight, 1)

    result_argmax = graph.get_tensor_by_name("result_argmax:0")

    prediction_class = sess.run(result_argmax, feed_dict = {x: miniBatch[0], keep_prob: 1.0})

    y = y_conv[0][prediction_class]

    gradient_saliency = saliency.GradientSaliency(graph, sess, y, x)




        # straight_guided_backprop_saliency = GuidedBackpropSaliency(graph, sess, straightBoard[1], straightBoard[0])
	# left_guided_backprop_saliency = GuidedBackpropSaliency(graph, sess, leftBoard[1], leftBoard[0])
	# right_guided_backprop_saliency = GuidedBackpropSaliency(graph, sess, rightBoard[1], rightBoard[0])

        # smoothgrad_guided_backprop = guided_backprop_saliency.GetSmoothedMask(straightBoard[0], feed_dict={x: straightBoard[0], y_:straightBoard[1], keep_prob: 1})
	# smoothgrad_guided_backprop =  guided_backprop_saliency.GetSmoothedMask(leftBoard[0], feed_dict={x: leftBoard[0], y_:leftBoard[1], keep_prob: 1})
	# smoothgrad_guided_backprop = guided_backprop_saliency.GetSmoothedMask(rightBoard[0], feed_dict={x: rightBoard[0], y_:rightBoard[1], keep_prob: 1})

# Compute a 2D tensor for visualization.
    grayscale_visualization = visualization.VisualizeImageGrayscale(smoothgrad_guided_backprop)

#    print("starting validation...")
  #  for miniBatch in batchGenerator(validationBatchData, 1):
#	validate_result = sess.run(result_argmax, feed_dict={
#            x: miniBatch[0],
#            keep_prob: 1.0})


    print("done")
    sys.stdout.flush()

"""
