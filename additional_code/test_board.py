import numpy as np
import time

#boards=np.load('/home/snake/data/snakeNN/snakeNN_code/presentation/snakeNN_0/trainingDataBoards1.npy')

# def drawBoard(boards, move, num):#indexArray, num):
# 	row = 0
# 	head=(-1,-1)
# 	for i in boards[num]:
# 		row = row + 1
# 		column = 0
# 		for j in i:
# 			column = column + 1
# 			if j == 0:
# 				print(" "),
# 			else:
# 				print(j),
# 				if j==4:
# 					head=(column,50 - row)
# 		print("")
# 	print("-------------------------------------------------------------")
# 	print("head: " + str(head))
# 	print ("move: "+ str(move[num]))
# 	#print ("index: " + str(indexArray[num]))

def drawBoard(boards, move, num, outFile):#indexArray, num):
	print("")
	row = 1
	column = 0
	head=(-1,-1)
	for i in range(2704):
		column = column + 1

		#if boards[num][i] == 7:
			#print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
			#print "                                                         " + str(num) + "                                                                         "
			#print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
			#time.sleep(10)

		if boards[num][i] == 0:
			print(" "),
			outFile.write(" ")
		else:
			print(boards[num][i]),
			outFile.write(str(boards[num][i]))
			if boards[num][i]==5:
				head=(row,column)

		if (i%52==51):
			row = row + 1
			column = 0
			print("")
			outFile.write("\n")

	print("")
	print("-------------------------------------------------------------")
	print("head: " + str(head))
	print ("move: "+ str(move[num]))
	#print ("index: " + str(indexArray[num]))

def drawLayerOne(boards, move, num):
	print("")
	row = 1
	column = 0
	head=(-1,-1)
	for i in range(2704):
		column = column + 1
		if boards[num][i] == 0:
			print(" "),
		elif boards[num][i] == 4:
			print(boards[num][i]),
		elif boards[num][i] == 5:
			print(boards[num][i]),
			head=(row, column)
		else:
			print(" "),

		if (i%52==51):
			row = row + 1
			column = 0
			print("")
	print("")
	print("____________________________________________________________")
	print("head: " + str(head))
	print("move: " + str(move[num]))


def drawLayerTwo(boards, move, num):
        print("")
        row = 1
        column = 0
        head=(-1,-1)
        for i in range(2704):
                column = column + 1
                if boards[num][i] == 0:
                        print(" "),
                elif boards[num][i] == 3:
                        print(boards[num][i]),
		elif boards[num][i] == 1:
			print(boards[num][i]),
		elif boards[num][i] == 2:
			print(boards[num][i]),
                elif boards[num][i] == 5:
			print(" "),
                        head=(row, column)
		else:
			print(" "),

                if (i%52==51):
                        row = row + 1
                        column = 0
                        print("")
        print("")
        print("____________________________________________________________")
        print("head: " + str(head))
        print("move: " + str(move[num]))


def drawLayerThree(boards, move, num):
        print("")
        row = 1
        column = 0
        head=(-1,-1)
        for i in range(2704):
                column = column + 1
                if boards[num][i] == 0:
                        print(" "),
                elif boards[num][i] == 6:
                        print(boards[num][i]),
                elif boards[num][i] == 7:
                        print(boards[num][i]),
                elif boards[num][i] == 5:
			print(" "),
                        head=(row, column)
		else:
			print(" "),

                if (i%52==51):
                        row = row + 1
                        column = 0
                        print("")
        print("")
        print("____________________________________________________________")
        print("head: " + str(head))
        print("move: " + str(move[num]))


def movieBoard(allBoards, move): #, indexArray):
	print ("starting movie")
	for i in range(0,len(allBoards)):
		drawBoard(allBoards, move, i)#indexArray, i)
		time.sleep(0.5)
