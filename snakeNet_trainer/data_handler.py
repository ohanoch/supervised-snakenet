import numpy as np
from settings import fileLocation, innerFolder, amountOfMiniBatchFilesToTrain, biasRatio
import os

class DataHandler:
	def __init__(self):
		self.validationBatchData = []
		self.validationBatchDataBiased = []
		self.straightAllBoardFiles = []
		self.straightAllMoveFiles = []
		self.leftAllBoardFiles = []
		self.leftAllMoveFiles = []
		self.rightAllBoardFiles = []
		self.rightAllMoveFiles = []
		self.batchData = []
		self.testData = []

	def load_test_data(self, file_num):
		self.testData =\
			[np.load(fileLocation + "testData/" + innerFolder + 'testDataBoards' + str(file_num) + ".npy"),
			np.load(fileLocation + "testData/" + innerFolder + 'testDataMoves' + str(file_num) + ".npy")]

	def prepare_test_data(self, i):
		self.load_test_data(i)
		
	def load_validation_data(self):
		validationBatchDataStraight =\
			[np.load(\
				fileLocation + "validationData/" + 'validationDataBoardsStraight' + str(1) + ".npy"),\
			np.load(\
				fileLocation + "validationData/" + 'validationDataMovesStraight' + str(1) + ".npy")]

		validationBatchDataLeft =\
			[np.load(\
				fileLocation + "validationData/"+ 'validationDataBoardsLeft' + str(1) + ".npy"),\
			np.load(\
				fileLocation + "validationData/" + 'validationDataMovesLeft' + str(1) + ".npy")]

		validationBatchDataRight =\
			[np.load(\
				fileLocation + "validationData/" + 'validationDataBoardsRight' + str(1) + ".npy"),\
			np.load(\
				fileLocation + "validationData/"  + 'validationDataMovesRight' + str(1) + ".npy")]

		self.validationBatchDataBiased =\
			[np.load(\
				"/mnt/snake/snakeNN/snakeNN_data11/validationData/" +\
				innerFolder + 'validationDataBoards' + str(1) + ".npy"),\
			np.load(\
				"/mnt/snake/snakeNN/snakeNN_data11/validationData/" +\
				 innerFolder + 'validationDataMoves' + str(1) + ".npy")]

		return validationBatchDataStraight, validationBatchDataLeft, validationBatchDataRight

	def prepare_validation_data(self):
		validationBatchDataStraight, validationBatchDataLeft, validationBatchDataRight =\
																					self.load_validation_data()

		validationDataShuffle =\
			 np.array(range(len(validationBatchDataStraight[0]) +\
			 len(validationBatchDataLeft[0]) +\
			 len(validationBatchDataRight[0])))
		np.random.shuffle(validationDataShuffle)
		self.validationBatchData = [\
			np.concatenate(\
				(validationBatchDataStraight[0],\
				validationBatchDataLeft[0],\
				validationBatchDataRight[0]))[validationDataShuffle],\
			np.concatenate(
				(validationBatchDataStraight[1],\
				validationBatchDataLeft[1],\
				validationBatchDataRight[1]))[validationDataShuffle]]

	def load_training_data(self,\
		straightBoardFile,\
		straightMoveFile,\
		leftBoardFile,\
		leftMoveFile,\
		rightBoardFile,\
		rightMoveFile):

		batchDataStraight =\
			[np.concatenate([\
				np.load(fileLocation + "trainingData/" + "straight/" + boardFile)\
				for boardFile in straightBoardFile]),\
			np.concatenate([\
				np.load(fileLocation + "trainingData/" + "straight/" + moveFile)\
				for moveFile in straightMoveFile])]

		batchDataLeft =\
			[np.load(fileLocation + "trainingData/"+ "left/" + leftBoardFile),\
			np.load(fileLocation + "trainingData/" + "left/" + leftMoveFile)]

		batchDataRight =\
			[np.load(fileLocation + "trainingData/"+ "right/" + rightBoardFile),\
			np.load(fileLocation + "trainingData/" + "right/" + rightMoveFile)]

		return batchDataStraight, batchDataLeft, batchDataRight

	def ls_training_data(self):
		straightAllFiles = sorted(os.listdir(fileLocation + "trainingData/"+ "straight/"))
		leftAllFiles = sorted(os.listdir(fileLocation + "trainingData/"+ "left/"))
		rightAllFiles = sorted(os.listdir(fileLocation + "trainingData/"+ "right/"))

		self.straightAllBoardFiles = straightAllFiles[:int(len(straightAllFiles)/2)]
		self.straightAllMoveFiles = straightAllFiles[int(len(straightAllFiles)/2):]
		self.leftAllBoardFiles = leftAllFiles[:int(len(leftAllFiles)/2)]
		self.leftAllMoveFiles = leftAllFiles[int(len(leftAllFiles)/2):]
		self.rightAllBoardFiles = rightAllFiles[:int(len(rightAllFiles)/2)]
		self.rightAllMoveFiles = rightAllFiles[int(len(rightAllFiles)/2):]

		straightFileShuffle = np.array(range(len(self.straightAllBoardFiles)))
		np.random.shuffle(straightFileShuffle)
		self.straightAllBoardFiles = np.array(self.straightAllBoardFiles)[straightFileShuffle]
		self.straightAllMoveFiles = np.array(self.straightAllMoveFiles)[straightFileShuffle]
		self.straightAllBoardFiles = self.straightAllBoardFiles[:amountOfMiniBatchFilesToTrain * biasRatio]
		self.straightAllMoveFiles = self.straightAllMoveFiles[:amountOfMiniBatchFilesToTrain * biasRatio]

		leftFileShuffle = np.array(range(len(self.leftAllBoardFiles)))
		np.random.shuffle(leftFileShuffle)
		self.leftAllBoardFiles = np.array(self.leftAllBoardFiles)[leftFileShuffle]
		self.leftAllMoveFiles = np.array(self.leftAllMoveFiles)[leftFileShuffle]
		self.leftAllBoardFiles = self.leftAllBoardFiles[:amountOfMiniBatchFilesToTrain]
		self.leftAllMoveFiles = self.leftAllMoveFiles[:amountOfMiniBatchFilesToTrain]

		rightFileShuffle = np.array(range(len(self.rightAllBoardFiles)))
		np.random.shuffle(rightFileShuffle)
		self.rightAllBoardFiles = np.array(self.rightAllBoardFiles)[rightFileShuffle]
		self.rightAllMoveFiles = np.array(self.rightAllMoveFiles)[rightFileShuffle]
		self.rightAllBoardFiles = self.rightAllBoardFiles[:amountOfMiniBatchFilesToTrain]
		self.rightAllMoveFiles = self.rightAllMoveFiles[:amountOfMiniBatchFilesToTrain]

		self.straightAllBoardFiles = np.array_split(self.straightAllBoardFiles, biasRatio)
		self.straightAllMoveFiles = np.array_split(self.straightAllMoveFiles, biasRatio)
		for i,_ in enumerate(self.straightAllBoardFiles):
			if self.straightAllBoardFiles[i].shape != self.straightAllBoardFiles[0].shape:
				self.straightAllBoardFiles[i] =\
					np.append(self.straightAllBoardFiles[i], random.choice(self.straightAllBoardFiles[i]))

		if (min(len(self.leftAllBoardFiles),len(self.rightAllBoardFiles)) < len(self.straightAllBoardFiles[0])):
			self.leftAllBoardFiles =\
				np.tile(self.leftAllBoardFiles ,\
					(int(len(self.straightAllBoardFiles[0])/len(self.leftAllBoardFiles)) + 1))
			self.leftAllMoveFiles =\
				np.tile(self.leftAllMoveFiles ,\
					(int(len(self.straightAllMoveFiles[0])/len(self.leftAllMoveFiles)) + 1))
			self.rightAllBoardFiles =\
				np.tile(self.rightAllBoardFiles ,\
					(int(len(self.straightAllBoardFiles[0])/len(self.rightAllBoardFiles)) + 1))
			self.rightAllMoveFiles =\
				np.tile(self.rightAllMoveFiles ,\
					(int(len(self.straightAllMoveFiles[0])/len(self.rightAllMoveFiles)) + 1))

		elif(min(len(self.leftAllBoardFiles),len(self.rightAllBoardFiles)) > len(self.straightAllBoardFiles[0])):
			self.straightAllBoardFiles =\
				 np.tile(self.straightAllBoardFiles ,\
					(int(max(len(self.leftAllBoardFiles),len(self.rightAllBoardFiles))/\
						len(self.straightAllBoardFiles[0])) + 1))
			self.straightAllMoveFiles =\
				np.tile(self.straightAllMoveFiles ,\
					(int(max(len(self.leftAllMoveFiles),len(self.rightAllMoveFiles))/\
						len(self.straightAllMoveFiles[0])) + 1))

		print("straights in first group: " + str(len(self.straightAllBoardFiles[0])) +\
			"\t straights in last group: " + str(len(self.straightAllBoardFiles[-1])))
		print("amount of straights: " +str(len(self.straightAllBoardFiles)*len(self.straightAllBoardFiles[0])) +\
			"\t amount of left: " + str(len(self.leftAllBoardFiles)) +\
			"\t amount of rights: " + str(len(self.rightAllBoardFiles)))

	def prepare_training_data(self,\
		straightBoardFile,\
		straightMoveFile,\
		leftBoardFile,\
		leftMoveFile,\
		rightBoardFile,\
		rightMoveFile):

		batchDataStraight, batchDataLeft, batchDataRight =\
			self.load_training_data(\
				straightBoardFile,
				straightMoveFile,\
				leftBoardFile,\
				leftMoveFile,\
				rightBoardFile,\
				rightMoveFile)
		
		dataShuffle =\
			np.array(range(len(batchDataStraight[0]) + len(batchDataLeft[0]) + len(batchDataRight[0])))
		np.random.shuffle(dataShuffle)

		self.batchData =\
			[np.concatenate((batchDataStraight[0] , batchDataLeft[0] , batchDataRight[0]))[dataShuffle] ,\
				np.concatenate((batchDataStraight[1] , batchDataLeft[1] , batchDataRight[1]))[dataShuffle]]


