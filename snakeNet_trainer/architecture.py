import tensorflow as tf

def deepnn_snakenet(x, keep_prob):
    #with tf.name_scope('reshape'):
    #    x_image = tf.reshape(x, [-1, 52, 52, 3])

    # First convolutional layer - maps one grayscale image to 32 feature maps.
    with tf.name_scope('conv1'):
        W_conv1 = weight_variable([5, 5, 3, 32]) #feature size 7x7 to have 46x46 image after convolution
        b_conv1 = bias_variable([32]) #32 feature maps - arbitrary - can change
        h_conv1 = tf.nn.relu(conv2d(x, W_conv1) + b_conv1) #uses max function (instead of sigmoid function)
        # h_conv1 = tf.nn.relu(conv2d(h_fc0_flat, W_conv1) + b_conv1)

    # Pooling layer - downsamples by 2X.
    with tf.name_scope('pool1'):
        h_pool1 = max_pool_2x2(h_conv1) #now size will be 23x23x32

    # Second convolutional layer -- maps 32 feature maps to 64.
    with tf.name_scope('conv2'):
        W_conv2 = weight_variable([3, 3, 32, 64]) #feature size 5x5 to have 19x19  image after convolution
        b_conv2 = bias_variable([64])
        h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
        # h_conv2 = tf.nn.relu(conv2d(h_conv1, W_conv2) + b_conv2)

    # Second pooling layer.
    with tf.name_scope('pool2'):
         h_pool2 = max_pool_2x2(h_conv2) #now size will be 9x9x64

    # Third convolutional layer -- maps 32 feature maps to 64.
    with tf.name_scope('conv3'):
         W_conv3 = weight_variable([3, 3, 64, 128]) #feature size 3x3 to have 7x7 image after convolution
         b_conv3 = bias_variable([128])
         h_conv3 = tf.nn.relu(conv2d(h_pool2, W_conv3) + b_conv3)

    # Fully connected layer 1 -- after 3rd convolutional layer we have 128 features images of size 7x7
    # is down to 10x10x64 feature maps -- maps this to 1024 features.
    with tf.name_scope('fc1'):
        W_fc1 = weight_variable([9 * 9 * 128,4096]) #4096 = first power of 2 larger than 2500 (=50x50), also (8*8*128 = 8192)/2
        b_fc1 = bias_variable([4096])

        # h_pool2_flat = tf.reshape(h_conv2, [-1, 10 * 10 * 64])
        h_conv3_flat = tf.reshape(h_conv3, [-1, 9 * 9 * 128])
        h_fc1 = tf.nn.relu(tf.matmul(h_conv3_flat, W_fc1) + b_fc1)

    # Dropout - controls the complexity of the model, prevents co-adaptation of
    # features.
    with tf.name_scope('dropout'): #maybe add dropout to other layers aswell?
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

    # Map the 4096 features to 3 classes, one for each direction
    with tf.name_scope('fc2'):
        W_fc2 = weight_variable([4096, 3])
        b_fc2 = bias_variable([3])

        # y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
        y_conv = tf.add(tf.matmul(h_fc1_drop, W_fc2), b_fc2, name="y_conv")

    return y_conv, keep_prob#, regularizer

def deepnn_alexnet(x, keep_prob):
    # First convolutional layer of strand one - maps images to 96 feature maps.
    with tf.name_scope('conv1_1'):
        W_conv1_1 = weight_variable([5, 5, 3, 48]) #feature size 5x5 to have 48x48 image after convolution
        b_conv1_1 = bias_variable([48]) #96 feature maps in total (48*2) network is siamese
        h_conv1_1 = tf.nn.relu(conv2d(x, W_conv1_1) + b_conv1_1) #uses ReLU function (better than sigmoid function for CNNs)

    # Pooling layer of strand one- downsamples by 2X.
    with tf.name_scope('pool1_1'):
        h_conv1_1 = tf.nn.local_response_normalization(h_conv1_1)
        h_pool1_1 = max_pool_2x2(h_conv1_1) #now size will be 24x24x48


    # Second convolutional layer of strand one -- maps 96 feature maps to 256.
    with tf.name_scope('conv2_1'):
        W_conv2_1 = weight_variable([3, 3, 48, 128]) #feature size 3x3 to have 22x22 image after convolution
        b_conv2_1 = bias_variable([128]) #total = 256 (still siamese)
        h_conv2_1 = tf.nn.relu(conv2d(h_pool1_1, W_conv2_1) + b_conv2_1)


    # Second pooling layer of strand one.
    with tf.name_scope('pool2_1'):
        h_conv2_1 = tf.nn.local_response_normalization(h_conv2_1)
        h_pool2_1 = max_pool_2x2(h_conv2_1) #now size will be 11x11x128


    # First convolutional layer of strand two - maps images to 96 feature maps.
    with tf.name_scope('conv1_2'):
        W_conv1_2 = weight_variable([5, 5, 3, 48])  # feature size 5x5 to have 48x48 image after convolution
        b_conv1_2 = bias_variable([48])  # 96 feature maps total (siamese)
        h_conv1_2 = tf.nn.relu(conv2d(x, W_conv1_2) + b_conv1_2)  # uses max function (instead of sigmoid function)


    # Pooling layer of strand two- downsamples by 2X.
    with tf.name_scope('pool1_2'):
        h_conv1_2 = tf.nn.local_response_normalization(h_conv1_2)
        h_pool1_2 = max_pool_2x2(h_conv1_2)  # now size will be 24x24x48


    # Second convolutional layer of strand two-- maps 96 feature maps to 256.
    with tf.name_scope('conv2_2'):
        W_conv2_2 = weight_variable([3, 3, 48, 128])  # feature size 3x3 to have 22x22 image after convolution
        b_conv2_2 = bias_variable([128]) #Total = 256 (still siamese)
        h_conv2_2 = tf.nn.relu(conv2d(h_pool1_2, W_conv2_2) + b_conv2_2)


    # Second pooling layer of strand two.
    with tf.name_scope('pool2_2'):
        h_conv2_2 = tf.nn.local_response_normalization(h_conv2_2)
        h_pool2_2 = max_pool_2x2(h_conv2_2)  # now size will be 11x11x128


    # Third convolutional layer of strand one-- maps 256 feature maps to 384.
    with tf.name_scope('conv3_1'):  #This layer reads output from conv 2 in both siamese strands. Hence has conv kernel of 3x3x256
        W_conv3_1 = weight_variable([3, 3, 256, 192]) #feature size 3x3 to have 9x9 image after convolution
        b_conv3_1 = bias_variable([192])
        h_conv3_1 = tf.nn.relu(conv2d(tf.concat([h_pool2_1, h_pool2_2],-1), W_conv3_1) + b_conv3_1)#Concatenate features from both siamese


    # Third convolutional layer of strand two-- maps 256 feature maps to 384.
    with tf.name_scope('conv3_2'):  ##This layer reads output from conv 2 in both siamese strands. Hence has conv kernel of 3x3x256
        W_conv3_2 = weight_variable([3, 3, 256, 192])  # feature size 3x3 to have 9x9 image after convolution
        b_conv3_2 = bias_variable([192])
        h_conv3_2 = tf.nn.relu(conv2d(tf.concat([h_pool2_1, h_pool2_2],-1), W_conv3_2) + b_conv3_2)#Concatenate features from both siamese


    # Fourth convolutional layer of strand one-- maps 384 feature maps to 384.
    with tf.name_scope('conv4_1'):
        W_conv4_1 = weight_variable([3, 3, 192, 192]) #feature size 3x3 to have 7x7 image after convolution
        b_conv4_1 = bias_variable([192])
        h_conv4_1 = tf.nn.relu(conv2d(h_conv3_1, W_conv4_1) + b_conv4_1)

# now 4x4x192

    # Fifth convolutional layer of strand one-- maps 32 feature maps to 64.
    with tf.name_scope('conv5_1'):
        W_conv5_1 = weight_variable([2, 2, 192, 128]) #feature size 2x2 to have 6x6 image after convolution
        b_conv5_1 = bias_variable([128])
        h_conv5_1 = tf.nn.relu(conv2d(h_conv4_1, W_conv5_1) + b_conv5_1)

# now 2x2x128

    # Third pooling layer of strand one.
    with tf.name_scope('pool3_1'):
		h_pool3_1 = max_pool_2x2(h_conv5_1) #now size will be 3x3x128

    # Fourth convolutional layer of strand two -- maps 384 feature maps to 384.
    with tf.name_scope('conv4_2'):
        W_conv4_2 = weight_variable([3, 3, 192, 192])  # feature size 3x3 to have 7x7 image after convolution
        b_conv4_2 = bias_variable([192])
        h_conv4_2 = tf.nn.relu(conv2d(h_conv3_2, W_conv4_2) + b_conv4_2)

    # Fifth convolutional layer of strand two-- maps 32 feature maps to 64.
    with tf.name_scope('conv5_2'):
        W_conv5_2 = weight_variable([2, 2, 192, 128])  # feature size 2x2 to have 6x6 image after convolution
        b_conv5_2 = bias_variable([128])
        h_conv5_2 = tf.nn.relu(conv2d(h_conv4_2, W_conv5_2) + b_conv5_2)

    # Third pooling layer of strand two.
    with tf.name_scope('pool3_2'):
        h_pool3_2 = max_pool_2x2(h_conv5_2)  # now size will be 3x3x128

        # now 6x6x256

    # Fully connected layer 1 -- after 2 round of downsampling, our 28x28 image
    # is down to 10x10x64 feature maps -- maps this to 1024 features.
    with tf.name_scope('fc1'): #256 is used for iput dim below as it takes in 128 inputs from the two siamese conv layers each (128*2)
        W_fc1 = weight_variable([3 * 3 * 256, 4096]) #4096 = first power of 2 larger than 2500 (=50x50)
        b_fc1 = bias_variable([4096])

        # h_pool2_flat = tf.reshape(h_conv2, [-1, 10 * 10 * 64])
        h_master_flat = tf.reshape(tf.concat((h_pool3_1, h_pool3_2), axis=-1), [-1, 3 * 3 * 256])
        h_fc1 = tf.nn.relu(tf.matmul(h_master_flat, W_fc1) + b_fc1)

    # Dropout - controls the complexity of the model, prevents co-adaptation of
    # features.
    with tf.name_scope('dropout1'): #maybe add dropout to other layers aswell?
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

    with tf.name_scope('fc2'):
        W_fc2 = weight_variable([4096, 4096]) #4096 = first power of 2 larger than 2500 (=50x50)
        b_fc2 = bias_variable([4096])

        # h_pool2_flat = tf.reshape(h_conv2, [-1, 10 * 10 * 64])
        h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

    # Dropout - controls the complexity of the model, prevents co-adaptation of
    # features.
    with tf.name_scope('dropout2'): #maybe add dropout to other layers aswell?
        h_fc2_drop = tf.nn.dropout(h_fc2, keep_prob)

    # Map the 4096 features to 3 classes, one for each direction
    with tf.name_scope('fc3'):
        W_fc3 = weight_variable([4096, 3])
        b_fc3 = bias_variable([3])

        # y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
        y_conv = tf.matmul(h_fc2_drop, W_fc3) + b_fc3

    return y_conv, keep_prob#, regularizer

def conv2d(x, W):
  """conv2d returns a 2d convolution layer with full stride."""
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID') #VALID = no padding


def max_pool_2x2(x):
  """max_pool_2x2 downsamples a feature map by 2X."""
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='VALID') #VALID = no padding

def weight_variable(shape):
  """weight_variable generates a weight variable of a given shape."""
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)


def bias_variable(shape):
  """bias_variable generates a bias variable of a given shape."""
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)









