# Based off of:
# http://ischlag.github.io/2016/06/12/async-distributed-tensorflow/
# https://www.tensorflow.org/deploy/distributed
# Aswell as tensorflow mnist tutorial for experts

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from utilities import *
from graph_builder import *
from session_trainer import *
from data_handler import *

import time
import sys
import datetime

import random
import os
import sys

import numpy as np
import tensorflow as tf

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string("nn_name", "snakenet", "specify name of nn, i.e snakenet or alexnet")

NUM_OF_BOARDS_IN_FILE = 20000

def main(_):
	print("\n\n --- STARTING CODE ---")
	print("------------------------")

	startTime = time.time()
	print_settings()

	graph = GraphBuilder(FLAGS.nn_name)
	graph.init_graph_nodes()
	sys.stdout.flush()
	graph.setup_graph_location()

	print("\nstarting session")
	sess_trainer = SessionTrainer()
	sess_trainer.graph = graph
	with tf.Session(config=sess_trainer.config) as sess:
		# sess_trainer.sess = sess
		saver = tf.train.Saver()
		sess.run(tf.global_variables_initializer())

		data = DataHandler()
		data.prepare_validation_data()

		print ("variables initialized, starting training...\n")
		for epoch in range(numEpochs):
			data.ls_training_data()	 

			global_step_start = sess_trainer.graph.global_step.eval()
			for i,\
				((straightBoardFile, straightMoveFile),\
				(leftBoardFile, leftMoveFile),\
				(rightBoardFile, rightMoveFile)) in\
				enumerate(zip(zip(zip(*data.straightAllBoardFiles), zip(*data.straightAllMoveFiles)),\
					zip(data.leftAllBoardFiles, data.leftAllMoveFiles),\
				zip(data.rightAllBoardFiles, data.rightAllMoveFiles))):

				loadDataTime = time.time()
				data.prepare_training_data(\
					straightBoardFile,\
					straightMoveFile,\
					leftBoardFile,\
					leftMoveFile,\
					rightBoardFile,\
					rightMoveFile)
				
				print("minibatch file: " + str(i) +\
					" epoch " + str(epoch) +\
					" started training.\tglobal step is: " + str(sess_trainer.graph.global_step.eval()) +\
					"\tlearning rate is: " + str(sess_trainer.graph.learning_rate.eval()) +\
					"\ttime passed: " + str(time.time() - startTime))
				sys.stdout.flush()	
				
				loadDataTime = time.time() - loadDataTime
				trainStartTime = time.time()
				gpuTotalTime = sess_trainer.train_minibatches(data.batchData)

				print ("minibatch file: " + str(i) + " epoch " +
					 str(epoch) + "\tload data time: "  +str(loadDataTime) +
					 "\ttotal train time: " + str(time.time()-trainStartTime) +
					 "\tGPU train time: " + str(gpuTotalTime))
				
				if(sess_trainer.graph.global_step.eval()-global_step_start)%\
					(((NUM_OF_BOARDS_IN_FILE/mini_batch_size)*(biasRatio+2))*validationFrequency)==0:
					print("global step: " + str(sess_trainer.graph.global_step.eval()) +\
					 " started validation on SLR. time passed: "+ str(time.time()-startTime))
					
					sess_trainer.validate_minibatches(data.validationBatchData, sess_trainer.graph.global_step)
					
					print("global step: " + str(sess_trainer.graph.global_step.eval()) +\
					 " started validation on biased data. time passed: "+ str(time.time()-startTime))
					
					sess_trainer.validate_minibatches(data.validationBatchDataBiased,\
						sess_trainer.graph.global_step)

					now = datetime.datetime.now()
					save_path = saver.save(sess, '../models/output_snake_model_fixed_genie' + model_type +\
									now.strftime("%Y%m%d_%H%M%S"),global_step=sess_trainer.graph.global_step)
					print("Model saved in file: %s" % save_path)

		trainEndTime = time.time()
		print ("training and validation ended. \t time it took: " + str(trainEndTime - startTime))
		print ("starting testing...")
		sys.stdout.flush()

		sess_trainer.test_minibatches(data)

		testEndTime = time.time()
		print("testing ended. \t time for testing: " + str(testEndTime - trainEndTime) +\
			 "\t total time: "+ str(testEndTime - startTime))
		sys.stdout.flush()

#		print("final confusion matrix:\n" + str(final_confusion))
#		sys.stdout.flush()

		now = datetime.datetime.now()
		save_path = saver.save(sess, '../models/output_snake_model_fixed_genie_final' +\
						 model_type + now.strftime("%Y%m%d_%H%M%S"))
		print("Model saved in file: %s" % save_path)
		sys.stdout.flush()


if __name__ == '__main__':

	tf.app.run(main=main)
