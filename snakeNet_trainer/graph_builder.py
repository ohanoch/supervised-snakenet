from architecture import *
from settings import starting_learning_rate
import tensorflow as tf
import numpy as np
import tempfile
import sys

class GraphBuilder:

	def __init__(self, nn_name):
		self.nn_name = nn_name

		self.global_step = tf.Variable(0, trainable=False)
		self.learning_rate =\
			 tf.train.exponential_decay(starting_learning_rate, self.global_step, 1000, 0.98, staircase=True)
		self.confusion = np.zeros([3,3])

		self.x = None
		self.y_ = None
		self.keep_prob = None
		self.y_conv = None
		self.argmax = None
		self.cross_entropy = None
		self.train_step = None
		self.accuracy = None
		self.confusion = None

	def init_graph_nodes(self):
		with tf.device('/gpu:0'):

			# Create the model
			self.x = tf.placeholder(tf.float32, [None, 52, 52, 3], name="x")

			# Define loss and optimizer
			self.y_ = tf.placeholder(tf.float32, [None, 3], name="y")

			self.keep_prob = tf.placeholder(tf.float32, name="keep_prob")
			# Build the graph for the deep net
			if self.nn_name == "snakenet":
				self.y_conv, self.keep_prob= deepnn_snakenet(self.x, self.keep_prob)
			elif self.nn_name == "alexnet":
				self.y_conv, self.keep_prob= deepnn_alexnet(self.x, self.keep_prob)

			self.argmax = tf.argmax(self.y_conv, 1, output_type=tf.int32, name="result_argmax")

			with tf.name_scope('loss'):
				self.cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=self.y_,
																		logits=self.y_conv)
			self.cross_entropy = tf.reduce_mean(self.cross_entropy)

			with tf.name_scope('adam_optimizer'):
				self.train_step = tf.train.\
					AdamOptimizer(self.learning_rate).\
					minimize(self.cross_entropy, global_step=self.global_step)

			with tf.name_scope('accuracy'):
				correct_prediction = tf.equal(tf.argmax(self.y_conv, 1), tf.argmax(self.y_, 1))
				correct_prediction = tf.cast(correct_prediction, tf.float32)

			self.accuracy = tf.reduce_mean(correct_prediction, name="accuracy1")
			self.confusion = tf.confusion_matrix(labels=tf.argmax(self.y_, 1),\
											predictions=tf.argmax(self.y_conv, 1),\
											num_classes=3,\
											name="confusion1")


	def setup_graph_location(self):
		graph_location = tempfile.mkdtemp()
		print('Saving graph to: %s' % graph_location)
		sys.stdout.flush()
		train_writer = tf.summary.FileWriter(graph_location)
		train_writer.add_graph(tf.get_default_graph())

