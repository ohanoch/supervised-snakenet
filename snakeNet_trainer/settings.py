amountOfMiniBatchFilesToTrain = 1#50
amountOfMiniBatchFilesToValidate = 1
amountOfMiniBatchFilesToTest = 2 #was 2
starting_learning_rate = 5*1e-4 #5*1e-4  #was 1e-3
mini_batch_size = 500   #was 500
numEpochs = 400
dataFileNumber = "" #14 #was 3, then 5
innerFolder = ""
keep_prob_start = 0.5
biasRatio = 2
# biasDecayFreq = 1
# biasRatioLimit = 4
validationFrequency = 1
model_type = "diffColor_yconv_"

fileLocation = "/mnt/snake/snakeNN/snakeNN_data14"+ str(dataFileNumber) +"/"
