from settings import *
import numpy as np

def batchGenerator(batchData, mini_batch_size): #border different color
    for batchStartIndex in range(0,len(batchData[0]), mini_batch_size):
        batchEndIndex = batchStartIndex + min(mini_batch_size, len(batchData[0]) - batchStartIndex)
        batch = [batchData[i][batchStartIndex:batchEndIndex] for i in range(2)]
        yield [\
            [np.reshape(np.array([\
                [pixel if (pixel == 1 or pixel == 2 or pixel == 3) else 0 for pixel in board],\
                [pixel if (pixel == 4 or pixel == 5) else 0 for pixel in board],\
                [pixel if (pixel == 6 or pixel == 7) else 0 for pixel in board]\
                ]).transpose(), (52,52,3)) for board in batch[0]], \
            batch[1]]

def print_settings():
    print("dataFileNumber: " + str(dataFileNumber))
    print("amountOfMiniBatchFilesToTrain: " + str(amountOfMiniBatchFilesToTrain))
    print("amountOfMiniBatchFilesToValidate: " + str(amountOfMiniBatchFilesToValidate))
    print("amountOfMiniBatchFilesToTest: " + str(amountOfMiniBatchFilesToTest))
    print ("starting learning rate: " + str(starting_learning_rate))
    print ("mini batch size: "+str(mini_batch_size))
    print ("epochs to be trained: " +str(numEpochs))
    print ("keep_prob_start: " + str(keep_prob_start))
    print ("starting bias ratio: " + str(biasRatio))
    print("model_type: " + str(model_type))
